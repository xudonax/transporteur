# syntax=docker/dockerfile:1

FROM    python:3.8-slim-buster

WORKDIR /transporteur

COPY    requirements.txt requirements.txt
RUN     pip3 install -r requirements.txt

COPY    . .

run     apt-get update && \
        apt-get install -y gettext
run     locales/generate_messages.sh

CMD     ["python3", "-u", "main.py"]
