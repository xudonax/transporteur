from peewee import CharField
from enum import Enum

class EnumField(CharField):
    """
    This class enables an Enum like field for Peewee
    """
    def __init__(self, choices: Enum, *args, **kwargs):
        super(CharField, self).__init__(*args, **kwargs)
        self.choices = choices
        self.max_length = 255

    def db_value(self, value):
        return value.name

    def python_value(self, value):
        return self.choices[value]
