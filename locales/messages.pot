# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-21 00:34+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "dadbot_reply"
msgstr ""

msgid "suggest_modal_title"
msgstr ""

msgid "suggest_modal_description"
msgstr ""

msgid "suggest_modal_placeholder"
msgstr ""

msgid "suggest_modal_submit"
msgstr ""

msgid "suggest_thread_title"
msgstr ""

msgid "suggest_suggest_cmd"
msgstr ""

msgid "suggest_suggest_description"
msgstr ""

msgid "suggest_accept_cmd"
msgstr ""

msgid "suggest_accept_description"
msgstr ""

msgid "suggest_reject_cmd"
msgstr ""

msgid "suggest_reject_description"
msgstr ""

msgid "suggest_run_in_thread"
msgstr ""

msgid "suggest_accepted"
msgstr ""

msgid "suggest_rejected"
msgstr ""

msgid "suggest_state_in_progress"
msgstr ""

msgid "suggest_state_timed_out"
msgstr ""

msgid "suggest_state_accepted"
msgstr ""

msgid "suggest_state_rejected"
msgstr ""

msgid "suggest_message"
msgstr ""

msgid "count_command"
msgstr ""

msgid "count_group_command_description"
msgstr ""

msgid "highscore_command_name"
msgstr ""

msgid "highscore_command_description"
msgstr ""

msgid "success_top_command_name"
msgstr ""

msgid "succes_top_command_description"
msgstr ""

msgid "failure_top_command_name"
msgstr ""

msgid "failure_top_command_description"
msgstr ""

msgid "highscore_command_response"
msgstr ""

msgid "success_top_command_response"
msgstr ""

msgid "failure_top_command_response"
msgstr ""

msgid "count_fail_twice"
msgstr ""

msgid "count_fail_response"
msgstr ""
