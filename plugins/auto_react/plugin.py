import discord
import plugin

from discord.ext import commands
from configparser import ConfigParser

# ========== Define plugin ==========

class Plugin(plugin.Plugin):

    # ========== Setup function ==========

    def setup(self, bot: commands.Bot):
        self.bot = bot
        self.enabled = bot.config["auto_react"]["enabled"].lower() == "true"
        if not self.enabled:
            return
        self.channel = discord.Object(id = int(bot.config["auto_react"]["channel_id"]))
        # TODO: Make work with custom emoji
        self.emoji = discord.PartialEmoji(name = bot.config["auto_react"]["emoji"])

    def load_defaults(self, config: ConfigParser):
        config["auto_react"] = {
            "enabled": True,
            "channel_id": "<channel id>",
            "emoji": "🤩"
        }

    def register_events(self, client: discord.Client):
        if self.enabled:
            client.add_listener(self.on_message)

    # ========== Event handlers ==========

    async def on_message(self, message: discord.Message):
        if (message.author.id == self.bot.user.id):
            return

        # Check if correct channel
        if (message.channel.id == self.channel.id):
            for attachment in message.attachments:
                if attachment.content_type.startswith("image"):
                    await message.add_reaction(self.emoji)
                    break
