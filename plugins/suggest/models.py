from peewee import Model, IntegerField, CharField
from custom_datatypes import EnumField
from enum import Enum

# ========== Define models ==========

class SuggestSuggestion(Model):
    class States(Enum):
        IN_PROGRESS = 1
        TIMED_OUT = 2
        ACCEPTED = 3
        REJECTED = 4

    message_id = IntegerField()
    user_id = IntegerField()
    state = EnumField(choices = States, default = States.IN_PROGRESS)
    description = CharField()
