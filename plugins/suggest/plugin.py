import discord
import plugin

from discord import app_commands
from discord.ext import commands
from discord import ui
from .models import SuggestSuggestion
from configparser import ConfigParser

# ========== Define suggestion modal =========

class SuggestModal(ui.Modal, title = _("suggest_modal_title")):
    description = ui.TextInput(
        label = _("suggest_modal_description"),
        required = True,
        placeholder = _("suggest_modal_placeholder"),
        style = discord.TextStyle.paragraph
    )

    def __init__(self, plugin: plugin.Plugin):
        super().__init__()
        self.plugin = plugin

    async def on_submit(self, interaction: discord.Interaction):
        await interaction.response.send_message(_("suggest_modal_submit"), ephemeral = True)

        # Create message
        message = await self.plugin.channel.send(".")
        # Create thread
        thread = await message.create_thread(
            name = _("suggest_thread_title").format(user=interaction.user.name),
            auto_archive_duration = 10080
        )

        # Save to database
        suggestion = SuggestSuggestion(
            message_id = message.id,
            user_id = interaction.user.id,
            description = str(self.description)
        )
        suggestion.save()

        # Populate message
        await self.plugin.update_message(message.id)

        # Add vote buttons
        await message.add_reaction(self.plugin.yes_emoji)
        await message.add_reaction(self.plugin.no_emoji)

# ========== Define plugin ==========

class Plugin(plugin.Plugin):

    # ========== Setup function ==========

    def setup(self, bot: commands.Bot):
        self.bot = bot
        self.enabled = bot.config["suggest"]["enabled"]
        self.channel_id = int(bot.config["suggest"]["channel_id"])

        # TODO: Make work with custom emoji
        self.yes_emoji = discord.PartialEmoji(name = bot.config["suggest"]["vote_yes"])
        self.no_emoji = discord.PartialEmoji(name = bot.config["suggest"]["vote_no"])

    def load_defaults(self, config: ConfigParser):
        config["suggest"] = {
            "enabled": True,
            "channel_id": "<channel id>",
            "vote_yes": '✅',
            "vote_no": '❌'
        }

    def register_events(self, client: discord.Client):
        if self.enabled:
            client.add_listener(self.on_ready)

    def register_commands(self, tree: app_commands.CommandTree):
        if self.enabled:
            tree.add_command(app_commands.Command(name=_("suggest_suggest_cmd"), description = _("suggest_suggest_description"), callback = self.suggest_cmd))
            tree.add_command(app_commands.Command(name=_("suggest_accept_cmd"), description = _("suggest_accept_description"), callback = self.accept_cmd))
            tree.add_command(app_commands.Command(name=_("suggest_reject_cmd"), description = _("suggest_reject_description"), callback = self.reject_cmd))

    # ========== Define commands ==========

    async def suggest_cmd(self, interaction: discord.Interaction):
        await interaction.response.send_modal(SuggestModal(self))

    async def accept_cmd(self, interaction: discord.Interaction):
        suggestion = SuggestSuggestion.get_or_none(SuggestSuggestion.message_id == interaction.channel_id)
        if suggestion == None:
            await interaction.response.send_message(_("suggest_run_in_thread"), ephemeral = True)
            return
        suggestion.state = SuggestSuggestion.States.ACCEPTED
        suggestion.save()
        await interaction.response.send_message(_("suggest_accepted"))
        await self.update_message(suggestion.message_id)


    async def reject_cmd(self, interaction: discord.Interaction):
        suggestion = SuggestSuggestion.get_or_none(SuggestSuggestion.message_id == interaction.channel_id)
        if suggestion == None:
            await interaction.response.send_message(_("suggest_run_in_thread"), ephemeral = True)
            return
        suggestion.state = SuggestSuggestion.States.REJECTED
        suggestion.save()
        await interaction.response.send_message(_("suggest_rejected"))
        await self.update_message(suggestion.message_id)

    # ========== Helper functions ==========
    def enum_to_name(self, value: SuggestSuggestion.States) -> str:
        if value == SuggestSuggestion.States.IN_PROGRESS:
            return _("suggest_state_in_progress")
        elif value == SuggestSuggestion.States.TIMED_OUT:
            return _("suggest_state_timed_out")
        elif value == SuggestSuggestion.States.ACCEPTED:
            return _("suggest_state_accepted")
        elif value == SuggestSuggestion.States.REJECTED:
            return _("suggest_state_rejected")

    async def update_message(self, message_id: int):
        # Get suggestion from database
        suggestion = SuggestSuggestion.get(SuggestSuggestion.message_id == message_id)
        # Get message
        message = await self.channel.fetch_message(message_id)
        # Edit message
        await message.edit(content = _("suggest_message").format(user=f"<@{suggestion.user_id}>", description=suggestion.description, state=self.enum_to_name(suggestion.state)))

    # ========== Event handlers ==========

    async def on_ready(self):
        self.channel = self.bot.get_channel(self.channel_id)
