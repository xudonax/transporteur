from peewee import Model, IntegerField

# ========== Define models ==========

class CountUser(Model):
    user_id = IntegerField(primary_key = True)
    success_count = IntegerField(default = 0)
    failure_count = IntegerField(default = 0)
    high_score = IntegerField(default = 0)

