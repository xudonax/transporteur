import discord
import plugin
import random
import re

from discord.ext import commands
from configparser import ConfigParser

# ========== Define plugin ==========

class Plugin(plugin.Plugin):

    # ========== Setup function ==========

    def setup(self, bot: commands.Bot):
        self.bot = bot
        self.enabled = bot.config["dadbot"]["enabled"].lower() == "true"
        self.channel_blacklist = [int(channel) for channel in bot.config["dadbot"]["channel_blacklist"].split(',')]
        self.trigger = bot.config["dadbot"]["trigger"].lower()
        self.chance = float(bot.config["dadbot"]["chance"])

    def load_defaults(self, config: ConfigParser):
        config["dadbot"] = {
            "enabled": True,
            "channel_blacklist": "",
            "trigger": "I am",
            "chance": "5"
        }

    def register_events(self, client: discord.Client):
        if self.enabled:
            client.add_listener(self.on_message)

    # ========== Event handlers ==========

    async def on_message(self, message: discord.Message):
        if (message.author.id == self.bot.user.id):
            return

        # Check if correct channel
        if (message.channel.id not in self.channel_blacklist):
            # Check if correct trigger
            m = re.search(f"^{self.trigger} ([^,.\n]*)", message.content, re.I)
            if (m != None):
                # Random chance
                if (random.randint(0, 100) < self.chance):
                    # Dad time!
                    content = m.group(1)[:32]
                    try:
                        await message.author.edit(nick = content)
                        await message.reply(_("dadbot_reply").format(content=f"<@{message.author.id}>", bot=self.bot.user.name))
                    except:
                        content = m.group(1) # Here we're not limited by the nickname length
                        await message.reply(_("dadbot_reply").format(content=content, bot=self.bot.user.name))
